# Kontrol Kullanım Asistanı #

* Yazar: Joseph Lee
* İndir: [versiyon 2.1][1]

Mevcut odak hakkında kısa bir yardım mesajı dinlemek için bu eklentiyi
kullanın.  Onay kutuları, yazı alanları vb Odaktaki kontrolle nasıl
etkileşime geçeceğinizle ilgili kısa bir mesaj için NvDA+H tuşlarına basın.

## 2.1 ##

* Yeni ve güncellenmiş çeviriler.


## 2.0 ##

* Terminal pencereleri dahil, daha fazla kontrol için yardım mesajı eklendi.
* Microsoft Excel ve Powerpoint ve Windows 8 başlangıç ​​ekranı gibi bazı
  uygulamalar için yardım mesajları eklendi.
* (Internet Explorer, Adobe Reader, Mozilla Firefox, vb) uygulamalar için,
  tarama ve odak kiplerine dair yardım mesajları eklendi
* Yeni dil: Danimarkaca.


## 1.0 ##

* İlk versiyon.

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
