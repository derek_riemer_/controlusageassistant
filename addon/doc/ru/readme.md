# Control Usage Assistant #

* Автор: Joseph Lee
* Загрузить: [version 2.1][1]

Используйте это дополнение, чтобы узнать, как взаимодействовать с типом
управления в фокусе. Нажмите NVDA+H, чтобы получить короткую справку по
взаимодействию с определённым типом управления, например, флажки, поля
редактирования и так далее.

## 2.1 ##

* Новые и обновленные переводы.


## 2.0 ##

* добавлены несколько сообщений для некоторых элементов управления, в том
  числе окна терминала.
* Добавлены сообщения справки для работы в некоторых областях приложений,
  таких как Microsoft Excel и Powerpoint и стартовый экран Windows 8.
* Добавлены сообщения справки для работы с формами в обоих режимах, обзора и
  редактирования, в виртуальных буферах документов (Internet Explorer, Adobe
  Reader, Mozilla Firefox и др.).
* Новый язык: датский.


## 1.0 ##

* Начальная версия.

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
