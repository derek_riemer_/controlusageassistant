# Asystent używania kontrolek / Control Usage Assistant #

* Autor: Joseph Lee
* Pobierz: [wersja 2.1][1]

Użyj tego dodatku, aby dowiedzieć się w jaki sposób pracować z aktualną
kontrolką.  Naciśnij NvDA+H aby uzyskać krótką pomoc dotyczącą używania
bieżącej kontrolki, (pola wyboru, pola edycji, itd.)

## 2.1 ##

* Nowe i zaktualizowane tłumaczenia.


## 2.0 ##

* Dodane instrukcje dla większej liczby kontrolek w tym dotyczące okna
  terminala.
* Dodane instrukcje jak pracować w niektórych obszarach aplikacji, w tym
  Microsoft Excel i Powerpoint oraz na ekranie startowym Windows 8.
* Dodane instrukcje jak pracować z formularzami w trybie czytania i trybie
  formularza (Internet Explorer, Adobe Reader, Mozilla Firefox, etc.).
* Nowy język: duński.


## 1.0 ##

* Pierwsza wersja.

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
