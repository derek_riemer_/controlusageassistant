# Control Usage Assistant #

* Autor: Joseph Lee
* Descarga: [versión 2.1][1]

Utiliza este complemento para alviscar como interactuar co control
enfocado. Preme NVDA + H para obter unha mensaxe curta de axuda na
interacción co control enfocado, como cas casiñas de verificación, campos de
edición e así con outros.

## 2.1 ##

* Traduccións novas e actualizadas.


## 2.0 ##

* Engadidas mensaxes de axuda para máis controis, incluindo a terminal de
  windows.
* Engadidas mensaxes para traballar nalgunhas áreas de aplicacións, como
  Microsoft Excel e PowerPoint e para a pantalla de inicio do Windows 8.
* Engadidas mensaxes de axuda para traballar cos formularios nos modos
  navegar e foco nos documentos do modo virtual (Internet Explorer, Adobe
  Reader, Mozilla Firefox, etc.).
* Nova lingua: Danés.


## 1.0 ##

* Versión inicial.

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
