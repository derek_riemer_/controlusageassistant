# Säätimen käyttöapu #

* Tekijä: Joseph Lee
* Lataa: [versio 2.1][1]

Tämän lisäosan avulla on mahdollista opiskella eri tapoja, joilla voidaan
olla vuorovaikutuksessa aktiivisen säätimen kanssa.  Paina NVDA+H
kuullaksesi lyhyen ohjeen aktiivisen säätimen käytöstä, kuten
esim. valintaruudun valitsemisesta, tekstin muokkaamisesta jne.

## 2.1 ##

* Uusia ja päivitettyjä käännöksiä.


## 2.0 ##

* Lisätty ohjeviestejä useammille säätimille, pääteikkunat
  (esim. komentokehote) mukaanlukien.
* Lisätty ohjeviestejä sovellusten, kuten Microsoft Excelin, Powerpointin
  sekä Windows 8:n aloitusnäytön tietyille osille.
* Lisätty ohjeviestejä lomakkeiden käyttämiseen asiakirjoissa sekä selaus-
  että vuorovaikutustiloissa (Internet Explorerissa, Adobe Readerissa,
  Mozilla Firefoxissa jne).
* Uusi kieli: tanska.


## 1.0 ##

* Ensimmäinen versio.

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
