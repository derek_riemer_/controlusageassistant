# Assistent voor besturingselementgebruik #

* Auteur: Joseph Lee
* Download: [version 2.1][1]

Gebruik deze add-on om uit te vinden hoe het besturingselement met focus
bediendmoet worden. Druk op NVDA+h om een korte helptekst te krijgen over
het element met focus zoals bijvoorbeeld selectievakjes, invoervelden, etc.

## 2.1 ##

* New and updated translations.


## 2.0 ##

* Helpberichten toegevoegd voor meer besturingselement, inclusief
  promptschermen.
* Helpmeldingen toegevoegd voor het werken in sommige gebieden van
  applicaties, zoals Microsoft Excel en Powerpoint en het Windows 8
  startscherm.
* Helpmeldingen toegevoegd voor het werken met formulieren zowel in blader-
  als focusmodus in virtuele buffer documenten (Internet Explorer, Adobe
  Reader, Mozilla Firefox, etc.).
* Nieuwe vertaling: Deens.


## 1.0 ##

* Eerste versie.

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
