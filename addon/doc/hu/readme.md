# Tájékoztató üzemmód #

* Készítő: Joseph Lee
* Letöltés: [2.1-es verzió][1]

A kiegészítő segítségével információt kaphat a kurzor alatt lévő elemről
(pl. hogyan jelölheti be a jelölőnégyzetet, stb). Használathoz csak nyomja
meg az NVDA+h billentyűparancsot.

## 2.1 ##

* Új és frissített fordítások.


## 2.01.0 ##

* Több súgóüzenet is hozzáadásra került, beleértve a terminál ablakhoz
  tartozókat.
* Több a munkát elősegítő programhoz rendeltek hozzá súgóüzenetet, úgy mint
  a Microsoft Excel, Powerpoint és Windows 8 kezdőképernyő.
* Súgó üzenetek hozzáadása az űrlapokkal dolgozó alkalmazásokhoz (Internet
  explorer, Adobe reader, Mozilla Firefox, stb).
* Új nyelv: Dán


## 1.0 ##

* Első verzió

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
