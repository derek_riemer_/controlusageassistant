# Hilfe zur Benutzung von Steuerelementen #

* Autor: Joseph Lee
* Download: [version 2.1][1]

Verwenden Sie diese Erweiterung, um zu erfahren, wie Sie das aktuell
hervorgehobene Steuerelement benutzen können. Drücken Sie NVDA+H, um eine
kurze Hilfestellung zur Verwendung des aktuell hervorgehobenen
Steuerelementes zu erhalten (eingeben von Text, aktivieren von
Kontrollkästchen etc.).

## 2.1 ##

* Neue und aktualisierte Sprachen.


## 2.0 ##

* Hilfemeldungen für mehr Elemente einschließlich Terminal-Fenster
  hinzugefügt.
* Hilfemeldungen für einige Bereiche in Anwendungen wie Microsoft Excel,
  Powerpoint und Windows 8 start-Bildschirm hinzugefügt.
* Hilfemeldungen für die Verwendung von Formularfeldern sowohl im Lese-Modus
  als auch im Fokus-Modus in virtuellen Dokumenten (Internet Explorer, Adobe
  Reader, Mozilla Firefox, etc.) hinzugefügt.
* Neue Sprache: dänisch.


## 1.0 ##

* anfängliche Version

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
