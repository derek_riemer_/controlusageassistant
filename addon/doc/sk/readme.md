# Pomoc pre prácu s prvkami #

* Autor: Joseph Lee
* Stiahnuť: [verzia 2.1][1]

Doplnok poskytuje pomoc pre aktuálne zameraný prvok. Aby ste počuli pomocné
hlásenie pre prvky, akými sú napríklad začiarkavacie alebo editačné polia ,
stlačte NVDA+h

## 2.1 ##

* Nové a aktualizované preklady.


## 2.0 ##

* pridané pomocné správy pre ďalšie prvky vrátane konzolových aplikácii.
* Pridané pomocné správy pre ďalšie časti v aplikáciách Microsoft Excel a
  Powerpoint a prihlasovacia obrazovka Windows 8.
* Pridané pomocné správy pre prácu s formulármi v režime fokusu a režime
  prehliadania v dokumentoch (Internet Explorer, Adobe Reader, Mozilla
  Firefox, atď.).
* pridaný dánsky preklad.


## 1.0 ##

* Prvé vydanie

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
