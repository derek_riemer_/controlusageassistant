# Control Usage Assistant #

* Autheur : Joseph Lee
* Télécharger : [version 2.1][1]

Utilisez ce module complémentaire pour savoir comment interagir avec
l'élément actif. Appuyez sur NVDA+H sur l'objet sur lequel vous souhaitez
avoir cette information.

## 2.1 ##

* Nouvelle langues et mises à jours d'autres


## 2.0 ##

* Messages d'aides ajoutés pour plus d'éléments, comme les fenêtres de
  consoles.
* Ajout de messages d'aide pour des zones d'applications, comme Microsoft
  Excel et Powerpoint ou l'écran d'accueil windows 8.
* Ajout de message pour les formulaires dans les navigateurs en mode
  formulaire ou moe navigation (Internet Explorer, Adobe Reader, Mozilla
  Firefox, etc.).
* Nouvelle langue : Danois


## 1.0 ##

* Première version

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
