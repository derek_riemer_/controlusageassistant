# Asistente para uso de controles #

* Autor: Joseph Lee
* Download: [version 2.1][1]

Use este complemento para descobrir como interagir com o controle em foco.
Pressione NVDA+H para obter uma mensagem curta de ajuda sobre como interagir
com o controle em foco, como caixas de seleção, campos de edição e assim por
diante.

## 2.1 ##

* New and updated translations.


## 2.0 ##

* Adicionadas mensagens de ajuda para mais controles, incluindo janelas de
  terminal.
* Adicionadas mensagens de ajuda para trabalhar com algumas áreas de
  aplicativos, como o Microsoft Excel e Powerpoint e a tela inicial do
  Windows 8.
* Adicionadas mensagens de ajuda para trabalhar com formulários em ambos os
  modos de navegação e de foco em documentos com exibidor virtual (Internet
  Explorer, Adobe Reader, Mozilla Firefox, etc.).
* Novo idioma: Dinamarquês.


## 1.0 ##

* Versão inicial.

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
