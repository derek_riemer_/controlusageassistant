# PomočnikZaUporaboKontrolnikov #

* Avtor: Joseph Lee
* Prenesi: [različico 2.1][1]

Uporabite ta dodatek kot pomoč pri ugotavljanju kako delati s kontrolnikom v
žarišču.  Pritisnite NvDA+H, da slišite kratko sporočilo pomoči kako
manipulirati s kontrolnikom v žarišču, kot so izbirniki, polja za urejanje
in drugi.

## 2.1 ##

* Novi in posodobljeni prevodi.


## 2.0 ##

* Dodana sporočila pomoči za več kontrolnikov vključno s terminalskimi okni.
* Dodana so bila sporočila pomoči za delo v posameznih odsekih programov,
  kot so Microsoft Excel, Microsoft PowerPoint in začetni zaslon Windows 8.
* Dodana so bila sporočila pomoči za delo z obrazci tako v brskalniškem kot
  v žariščnem delovanju v dokumentih iz navideznega predpomnilnika, kot so
  dokumenti v Internet Explorerju, Adobe Readerju, Mozilla Farefoxu in
  drugih programih.
* Nov jezik: Danščina.


## 1.0 ##

* Prva različica.

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
