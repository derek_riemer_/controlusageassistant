# Control Usage Assistant #

* Autor: Joseph Lee
* Descarga: [versión 2.1][1]

Utiliza este complemento para averiguar cómo interactuar con el control
enfocado.  Pulsa NVDA+H para obtener un breve mensaje de ayuda sobre la
interacción con el control enfocado, como con casillas de verificación,
campos de edición y así con otros.

## 2.1 ##

* Traducciones nuevas y actualizadas.


## 2.0 ##

* Añadidos mensajes de ayuda para más controles, incluyendo la terminal de
  windows.
* Añadidos mensajes de ayuda para el trabajo en algunas áreas de
  aplicaciones, tales como Microsoft Excel y PowerPoint y para la pantalla
  de inicio de Windows 8.
* Añadidos mensajes de ayuda para trabajar con formularios en modos
  navegación y foco en documentos en modo virtual (Internet Explorer, Adobe
  Reader, Mozilla Firefox, etc.).
* Nuevo idioma: Danés.


## 1.0 ##

* Versión inicial.

[[!tag stable]]

[1]: http://addons.nvda-project.org/files/get.php?file=cua
